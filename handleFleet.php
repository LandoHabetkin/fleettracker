<?php
include("startup.php");
$fleetObj = new fleet();
$router->get("/delete/{id}", function ($request, $response, $args) use ($fleetObj, $conn) {
    if ($fleetObj->del($args["id"])) {
        return $response->withStatus(301)->withHeader("Location", "/fleettracker/index.php");
    }
});

$router->run();