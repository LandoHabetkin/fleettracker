<?php

class fleet
{
    function __construct()
    {

    }

    public function add()
    {

    }

    public function edit()
    {

    }

    public function del($id)
    {
        global $conn;
        $id = (int)$id;

        echo "DELETE FROM fleets WHERE ID = $id LIMIT 1";
        $delFleet = $conn->query("DELETE FROM fleets WHERE ID = $id LIMIT 1");
        $delAttendants = $conn->query("DELETE FROM attendants WHERE fleet = $id");

        return true;
    }

    public function getAllFleets($limit = 0, $offset = 0)
    {
        global $conn;

        $stmt = $conn->prepare("SELECT * FROM fleets ORDER BY ID DESC LIMIT $limit OFFSET $offset");
        $stmt->execute();

        $fleets = array();

        while ($fleet = $stmt->fetch()) {
            if(!empty($fleet["mydate"])) {
                $fleet["timeStr"] = date("d.m.Y - H:i", $fleet["mydate"]);
                $userStmt = $conn->prepare("SELECT ID FROM attendants WHERE fleet = ?");
                $userStmt->execute(array($fleet["ID"]));

                $fleet["attendants"] = array();
                while ($attendant = $userStmt->fetch()) {
                    $myAttendant = $this->getAttendant($attendant["ID"]);
                    array_push($fleet["attendants"], $myAttendant);
                }

                array_push($fleets, $fleet);
            }
        }

        return $fleets;
    }
    public function getFleetsByFC($fcName, $limit, $offset){
        global $conn;
        $stmt = $conn->prepare("SELECT * FROM fleets WHERE FC = ? ORDER BY ID DESC LIMIT $limit OFFSET $offset");
        $stmt->execute(array($fcName));

        $fleets = array();

        while ($fleet = $stmt->fetch()) {
            $fleet["timeStr"] = date("d.m.Y - H:i", $fleet["mydate"]);
            $userStmt = $conn->prepare("SELECT ID FROM attendants WHERE fleet = ?");
            $userStmt->execute(array($fleet["ID"]));

            $fleet["attendants"] = array();
            while ($attendant = $userStmt->fetch()) {
                $myAttendant = $this->getAttendant($attendant["ID"]);
                array_push($fleet["attendants"],$myAttendant);
            }

            array_push($fleets, $fleet);
        }

        return $fleets;

    }
    public function getAttendant($id)
    {
        global $conn;
        $userStmt = $conn->prepare("SELECT * FROM attendants WHERE ID = ?");
        $userStmt->execute(array($id));

        $attendant = $userStmt->fetch();

        return $attendant;
    }
    public function countFleets()
    {
        global $conn;

        $stmt = $conn->prepare("SELECT COUNT(*) FROM fleets");
        $stmt->execute();
        $fleetCount = $stmt->fetch();
        $fleetCount = $fleetCount["COUNT(*)"];

        return $fleetCount;
    }

    function getCorpsInFleet(array $fleetAttendants)
    {
        $corps = array();
        foreach($fleetAttendants as $attendant)
        {
            $attendantCorp = $attendant["corp"];

            if(!isset($corps[$attendantCorp]))
            {
                $corps[$attendantCorp] = 1;
            }
            else
            {
                $corps[$attendantCorp] += 1;
            }
        }

        return $corps;
    }
}