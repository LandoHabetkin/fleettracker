<?php
function objectToArray($item)
{
    if (is_object($item)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $item = get_object_vars($item);
    }

    if (is_array($item)) {
        /*
        * Return array converted to object
        */
        return array_map(__FUNCTION__, $item);
    } else {
        // Return array
        return $item;
    }
}

function getArrayVal(array $array, $name)
{
    if (array_key_exists($name, $array)) {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Cache.SerializerPath', "./cache/");
        $purifier = new HTMLPurifier($config);
        if (!is_array($array[$name])) {
            $clean = $purifier->purify($array[$name]);
        } else {
            $clean = cleanArray($array[$name]);
        }
        return $clean;
    } else {
        return false;
    }
}

function cleanArray(array $theArray)
{
    $outArray = array();
    foreach ($theArray as $anArrayKey => $anArrayVal) {
        $outArray[$anArrayKey] = getArrayVal($theArray, $anArrayKey);
    }
    return $outArray;
}
