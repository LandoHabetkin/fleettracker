CREATE TABLE `attendants` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `fleet` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(255) NOT NULL,
  `ship` varchar(255) NOT NULL,
  `corp` varchar(255) NOT NULL,
  `alliance` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fleet` (`fleet`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `fleets` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `mydate` varchar(255) NOT NULL,
  `FC` varchar(255) NOT NULL,
  `fleettype` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
