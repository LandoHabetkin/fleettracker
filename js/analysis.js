var fleets = {
    el: "fleetList",
    url: "fleets.php/byCorp",
    dependencies: []
};

var fleetView = createView(fleets);
fleetView.afterUpdate(function () {
    var corpItems = fleetView.$get("items").corps;
    var fcItems = fleetView.$get("items").fcs;

    var totalParticipationItems = [];
    var participationRatioItems = [];
    for (var i = 0; i < corpItems.length; i++) {
        if (corpItems[i].name != "" && corpItems[i].attendants > 0) {
            var totalParticipationItem = {
                label: corpItems[i].name,
                value: corpItems[i].attendants
            };
            totalParticipationItems.push(totalParticipationItem);
        }
        if (corpItems[i].name != "" && corpItems[i].accountsInCorp > 0) {
            var participationRatioItem = {
                label: corpItems[i].name,
                value: corpItems[i].participationRatio
            };
            participationRatioItems.push(participationRatioItem);
        }


        var fcParticipationItems = [];
    }
    for (var j = 0; j < fcItems.length; j++) {

        var fcParticipationItem = {
            label: fcItems[j][0],
            value: fcItems[j][1]
        };
        fcParticipationItems.push(fcParticipationItem);

    }

    console.log(fcParticipationItems);
    var pie = new d3pie("chart", {
        header: {
            title: {
                text: "Total Participation"
            }
        },
        size: {
            canvasWidth: 650,
            canvasHeight: 400
        },
        data: {
            smallSegmentGrouping: {
                enabled: true,
                value: 3,
                valueType: "percentage",
                label: "Other",
                color: "#cccccc"
            },
            content: totalParticipationItems
        }
    });

    var fcPie = new d3pie("fcPieChart", {
        header: {
            title: {
                text: "FC Participation"
            }
        },
        size: {
            canvasWidth: 650,
            canvasHeight: 400
        },
        data: {
            smallSegmentGrouping: {
                enabled: true,
                value: 3,
                valueType: "percentage",
                label: "Other",
                color: "#cccccc"
            },
            content: fcParticipationItems
        }
    });

    nv.addGraph(function () {
        var chart = nv.models.discreteBarChart().x(function (d) {
            return d.label
        }).y(function (d) {
            return d.value
        });
        chart.staggerLabels(true);
        chart.showValues(true);
        chart.height(400);

        d3.select('#chart2 svg')
            .datum([{
                key: "bla",
                values: participationRatioItems
            }])
            .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
    });
});