function createView(myEl) {
    var myModel = {
        items: [],
        pages: [],
        limit: pagination.itemsPerPage,
        offset: 0,
        currentPage: 1,
        itemsCount: 0,
        update: myEl.dependencies,
        url: myEl.url
    };

    var vueview = new Vue({
        el: "#" + myEl.el,
        data: myModel,
        methods: {
            update: function (updateDependencies) {
                updateView(this, updateDependencies);
            },
            afterUpdate: function (updateHandler) {
                this.$on("iloaded", function () {
                    Vue.nextTick(updateHandler);
                });
            },
            afterLoad: function (updateHandler) {
                this.$once("iloaded", function () {
                    Vue.nextTick(updateHandler);
                });
            }
        }
    });

    jQuery.ajax({
        url: myEl.url,
        cache: false,
        beforeSend: function () {
            //startWait("progress" + myEl.el);
        },
        success: function (data) {

            var jsonData = JSON.parse(data)
            console.log(jsonData);
            myModel.items = jsonData.items;
            myModel.itemsCount = jsonData.count;
            myModel.pages = pagination.listPages(jsonData.count);
            vueview.$emit("iloaded");
        }
    });
    return vueview;
}

function updateView(view) {
    //get the URL from the view and modify it to add the limit and offset for the DB query
    var myUrl = view.url;

    myUrl += "" + view.limit

    myUrl += "/" + view.offset;


    console.log(myUrl);
    jQuery.ajax({
        url: myUrl,
        cache: false,
        beforeSend: function () {
            //startWait("progress" + myEl.el);
        },
        success: function (data) {
            console.log("loaded " + myUrl);
            console.log("loaded " + data);

            var responseData = JSON.parse(data);

            view.$set("items", responseData.items);

            Vue.set(view, "pages", pagination.listPages(responseData.count));

            var viewsToUpdate = view.$get("update");
            view.$emit("iloaded");

            if (viewsToUpdate.length > 0) {
                for (i = 0; i < viewsToUpdate.length; i++) {
                    console.log("updated: " + viewsToUpdate[i][0]);
                    updateView(viewsToUpdate[i], viewsToUpdate[i].url);
                }
            }
        }
    });
}
var pagination = {
    itemsPerPage: 10,
    listPages: function (numTotal) {
        //round up the number of pages
        var pagenum = Math.ceil(numTotal / this.itemsPerPage);
        var pages = [];

        //loop through the pages and create a page object for each page, having its index and limit
        for (var i = 0; i < pagenum; i++) {
            var index = i + 1;
            var page = {
                index: index,
                limit: index * this.itemsPerPage
            };
            pages.push(page);
        }

        return pages;

    },
    loadPage: function (view, page) {

        //calculate the offset for the DB query
        var offset = page * this.itemsPerPage - this.itemsPerPage;

        //set the new limit and offset to the view
        Vue.set(view, "limit", this.itemsPerPage);
        Vue.set(view, "offset", offset);

        //view.$set("limit", this.itemsPerPage);
        //view.$set("offset", offset);
        //update the current page for the view
        Vue.set(view, "currentPage", page);
        //view.$set("currentPage", page);

        //triger the view to be updated
        updateView(view, true);

    },
    loadNextPage: function (view) {
        //get current page
        var currentPage = view.$get("currentPage");
        //get total number of pages
        var numberOfPages = Math.ceil(view.$get("itemsCount") / this.itemsPerPage);

        //increment by one
        var nextPage = currentPage + 1;

        //if the next page would be beyond the last page, set nextPage to lastPage
        if (nextPage > numberOfPages) {
            nextPage = numberOfPages;
        }

        this.loadPage(view, nextPage);
    },
    loadPrevPage: function (view) {
        //get current page
        var currentPage = view.$get("currentPage");

        var nextPage = currentPage - 1;

        if (nextPage < 1) {
            nextPage = 1;
        }
        this.loadPage(view, nextPage);
    }
};

var paginationComponent = Vue.extend({
    props: ["view", "pages", "currentPage"],
    template: "<template v-if='pages.length > 1'>" +
    "<span id=\"paging\" style=\"margin-left:10px;\"> " +
    "<button class=\"btn btn-default\" style=\"float:none;font-size:9pt;margin:0 1px 0 1px;\" onclick=\"pagination.loadPrevPage({{view}})\"><<</button>" +
    "<span id=\"page{{page.index}}\" v-for=\"page in pages\" style=\"margin-left:2px;\"> " +
    "<button v-bind:class=\"currentPage == page.index ? 'btn btn-primary' : 'btn btn-default' \" " +
    "onclick=\"pagination.loadPage({{view}},{{page.index}});\">" +
    "{{page.index}}" +
    "</button> " +
    "</span> " +
    "<button class=\"btn btn-default\" style=\"float:none;font-size:9pt;margin:0 1px 0 1px;\" onclick=\"pagination.loadNextPage({{view}})\">>></button> " +
    "</span>" +
    "</template>"
});
//bind to <pagination> element
Vue.component("pagination", paginationComponent);
