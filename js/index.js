//Vue.config.silent = true;
//create the vue views, binding data to DOM elements

function filterFleets(view) {
    var fcSelect = document.getElementById("fcSelect");
    var fcName = fcSelect.options[fcSelect.selectedIndex].value;

    var typeSelect = document.getElementById("filterFleetType");
    var typeName = typeSelect.options[fcSelect.selectedIndex].value;

    var newUrl;
    if (fcName == "") {
        newUrl = "fleets.php/";
    }
    else if (fcName != "") {
        newUrl = "fleets.php/byFC/" + fcName + "/";
    }
    console.log(newUrl);
    Vue.set(view, "url", newUrl);
    updateView(view);

}
var fleets = {
    el: "fleetList",
    url: "fleets.php/",
    dependencies: []
};

pagination.itemsPerPage = 15;
var fleetView = createView(fleets);
fleetView.afterUpdate(function () {
    var fleetItems = fleetView.$get("items").fleets;

    var chartItems = [];
    for (var i = 0; i < fleetItems.length; i++) {
        var fleetItem = fleetItems[i];
        var fleetID = fleetItem.ID;

        var fleetShips = fleetItem.ships;
        var shipItems = [];
        for (var j = 0; j < fleetShips.length; j++) {
            shipItems.push({
                label: fleetShips[j][0],
                value: fleetShips[j][1]
            });
        }

        chartItems.push(shipItems);
        var chartElement = document.getElementById("chart"+i);
        var chart = new d3pie(chartElement, {
            header: {
                title: {
                    text: "Ship composition"
                }
            },
            size: {
                canvasWidth: 600,
                canvasHeight: 350
            },
            data: {
                smallSegmentGrouping: {
                    enabled: true,
                    value: 3,
                    valueType: "percentage",
                    label: "Other",
                    color: "#cccccc"
                },
                content: shipItems
            }
        });

    }

    var collapsibles = $("#accordion .collapse");
    collapsibles.collapse();

});