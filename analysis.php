<?php
$includeD3 = true;
include("header.php");
?>

<body>
<div class="container-fluid" id="fleetList">
    <h2>NoPap 1.0</h2>
    <ul class="nav nav-pills">
        <li><a href="index.php">Fleets</a></li>
        <li class="active"><a href="#">Analysis</a></li>
    </ul>
    <br/>
    <?php
    include("addFleetForm.php");
    ?>
    <h3>Analysis</h3>
    <h4>Breakdown by corp</h4>

    <div class="row">
        <div class="col-md-6">
            <div id="chart" style="text-align:center"></div>
        </div>
        <div class="col-md-6">
            <h5>Participation Ratio</h5>

            <div id="chart2" >
                <svg style="height:400px;"></svg>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div id="fcPieChart" style="text-align:center"></div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <table class="table table-responsive table-hover" >
                <tr>
                    <th>Corp</th>
                    <th>Total participation</th>
                    <th>Accounts in Corp</th>
                    <th>Participation ratio</th>
                </tr>
                <tr v-for="corp in items.corps">
                    <td>{{*corp.name}}</td>
                    <td>{{*corp.attendants}}</td>
                    <td>{{*corp.accountsInCorp}}</td>
                    <td>{{*corp.participationRatio}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="js/analysis.js"></script>

</html>