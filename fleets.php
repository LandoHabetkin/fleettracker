<?php
include("startup.php");
$fleetObj = new fleet();

//route to get paginated fleets
$router->get("/[{start}/{offset}]", function ($request, $response, $args) use ($fleetObj, $conn) {

    $offset = 0;
    if (isset($args["offset"])) {
        $offset = $args["offset"];
    }
    $limit = 15;
    if (isset($args["start"])) {
        $limit = $args["start"];
    }

    //get one page of fleets
    $fleets = $fleetObj->getAllFleets($limit, $offset);

    //list of FCs
    $fcs = [];

    //loop over fleets
    foreach ($fleets as &$fleet) {
        //add the fleet FCs to the list of fcs
        array_push($fcs, $fleet["FC"]);

        //list of shiptypes for this fleet
        $shipTypes = [];
        foreach ($fleet["attendants"] as $attendant) {
            array_push($shipTypes, $attendant["ship"]);
        }

        //list of unique ships for this fleet
        $ships = [];
        //count how often each ship occurs in this fleet
        $countShipTypes = array_count_values($shipTypes);
        //create an array with an entry for each ship, as well as its count
        foreach ($countShipTypes as $shipType => $shipTypeCount) {
            array_push($ships, [$shipType, $shipTypeCount]);
        }

        $fleet["ships"] = array_values($ships);
    }
    //remove duplicates
    $fcs = array_unique($fcs);

    $myItems["fleets"] = $fleets;
    //reindex
    $myItems["fcs"] = array_values($fcs);

    $myJson["items"] = $myItems;
    $myJson["count"] = $fleetObj->countFleets();

    echo json_encode($myJson);
});

$router->get("/byFC/{fc}/[{start}/{offset}]", function ($request, $response, $args) use ($fleetObj, $conn) {

    $offset = 0;
    if (isset($args["offset"])) {
        $offset = $args["offset"];
    }
    $limit = 15;
    if (isset($args["start"])) {
        $limit = $args["start"];
    }

    $fcName = $args["fc"];

    $fleets = $fleetObj->getFleetsByFC($fcName, $limit, $offset);

    $fcs = [];
    foreach ($fleets as &$fleet) {
        array_push($fcs, $fleet["FC"]);

        $shipTypes = [];
        foreach ($fleet["attendants"] as $attendant) {
            array_push($shipTypes, $attendant["ship"]);
        }

        $countShipTypes = array_count_values($shipTypes);
        $ships = [];
        foreach ($countShipTypes as $shipType => $shipTypeCount) {
            array_push($ships, [$shipType, $shipTypeCount]);
        }

        $fleet["ships"] = array_values($ships);
    }

    $fcs = array_unique($fcs);

    $myItems["fleets"] = $fleets;
    $myItems["fcs"] = array_values($fcs);

    $myJson["items"] = $myItems;
    $myJson["count"] = $fleetObj->countFleets();

    echo json_encode($myJson);
});


$router->get("/byCorp", function ($request, $response, $args) use ($fleetObj, $conn) {
    $fleets = $fleetObj->getAllFleets(1000);

    $allCorps = array();
    $allFCs = array();
    foreach ($fleets as $fleet) {
        $fleetCorps = $fleetObj->getCorpsInFleet($fleet["attendants"]);

        foreach ($fleetCorps as $corpName => $corpAttendants) {
            $corpFound = false;
            foreach ($allCorps as &$corp) {
                if ($corp["name"] == $corpName) {
                    $corp["attendants"] += $corpAttendants;
                    $corpFound = true;
                }
            }
            if (!$corpFound) {
                array_push($allCorps, array("name" => $corpName, "attendants" => $corpAttendants));
            }

        }
        array_push($allFCs, $fleet["FC"]);

    }

    foreach ($allCorps as &$corp) {
        $randomAccountsIncorp = rand(1, 25);

        $participationRatio = round($corp["attendants"] / $randomAccountsIncorp, 2);
        $corp["participationRatio"] = $participationRatio;
        $corp["accountsInCorp"] = $randomAccountsIncorp;
    }

    //$allCorps = array_unique($allCorps);

    $countFcs = array_count_values($allFCs);
    $fcs = [];
    foreach ($countFcs as $fc => $fleetcount) {
        array_push($fcs, [$fc, $fleetcount]);
    }

    $myItems["corps"] = $allCorps;
    $myItems["fcs"] = $fcs;

    $myFleets["items"] = $myItems;
    $myFleets["count"] = count($allCorps);
    echo json_encode($myFleets);
});

$router->post("/addFleetData", function ($request, $response, $args) use ($fleetObj, $conn) {
    // create pheal object with default values
    // so far this will not use caching, and since no key is supplied
    // only allow access to the public parts of the EVE API
    $pheal = new Pheal\Pheal();

    //split fleet comp into discrete lines
    $fleetCompLines = explode("\n", $_POST["fleetcomp"]);
    $fleetComp = array();

    //iterate lines and create array representing the values in each line
    foreach ($fleetCompLines as $line) {
        $lineArr = explode("\t", trim($line));
        array_push($fleetComp, $lineArr);
    }

    //create fleet
    $cleanPost = cleanArray($_POST);

    $insStmt = $conn->prepare("INSERT INTO fleets (FC,mydate,fleettype) VALUES (?, ?, ?)");
    $insStmt->execute(array($cleanPost["fc"], strtotime($cleanPost["date"]), $cleanPost["type"]));

    $fleetID = $conn->lastInsertId();
    $characterID = 0;
    //loop over attendants
    foreach ($fleetComp as &$attendant) {
        // requests /server/ServerStatus.xml.aspx
        if ($attendant[0]) {
            try {
                //get the character ID from the toon name
                $characterID = $pheal->eveScope->CharacterID(array("names" => $attendant[0]));
                $characterID = $characterID->characters[0]->characterID;
            } catch (\Pheal\Exceptions\PhealException $e) {
                echo sprintf(
                    "an exception was caught! Type: %s Message: %s",
                    get_class($e),
                    $e->getMessage()
                );
            }
        }

        //if a character ID was returned from the API
        if ($characterID) {
            try {
                //get the character sheet as per the character ID queried from the toon name
                $characterInfo = $pheal->eveScope->CharacterInfo(array("characterID" => $characterID));
                $characterCorp = $characterInfo->corporation;
                $characterAlliance = $characterInfo->alliance;

                //insert into database
                $insStmt = $conn->prepare("INSERT INTO attendants (fleet,name,location,ship,corp,alliance) VALUES (?, ?, ?, ?, ?, ?)");
                $insStmt->execute(array($fleetID, htmlentities($attendant[0]), htmlentities($attendant[1]), htmlentities($attendant[2]), $characterCorp,
                    $characterAlliance));
            } catch (\Pheal\Exceptions\PhealException $e) {
                echo sprintf(
                    "an exception was caught! Type: %s Message: %s",
                    get_class($e),
                    $e->getMessage()
                );
            }


        }
    }
    return $response->withStatus(301)->withHeader("Location", "/fleettracker/index.php");

});



$router->run();

