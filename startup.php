<?php
ini_set("arg_separator.output", "&amp;");
ini_set('default_charset', 'utf-8');
ob_start('ob_gzhandler');
header("Content-Security-Policy:default-src 'self'; style-src 'self' 'unsafe-inline';frame-src 'self'");

//require dependencies
require_once(__DIR__ . "/vendor/autoload.php");
require_once(__DIR__ . "/include/helpers.php");
require_once(__DIR__ . "/config.php");
// require html purifier
require_once(__DIR__ . "/include/HTMLPurifier.standalone.php");
use Pheal\Pheal;
use Pheal\Core\Config;
// setup file cache - CCP wants you to respect their cache timers, meaning
// some of the API Pages will return the same data for a specific while, or worse
// an error. If you use one of the availabe caching implementations,
// pheal will do the caching transparently for you.
// in this example we use the file cache, and configure it so it will write the cache files
// to /tmp/phealcache
Config::getInstance()->cache = new \Pheal\Cache\FileStorage(__DIR__ . '/cache/eveAPI/');

// Autoload requires classes on new class()
function myAutoload($class_name)
{
    $path = __DIR__ . "/include/class." . $class_name . ".php";
    if (file_exists($path)) {
        require_once($path);
    } else {
        return false;
    }
}
spl_autoload_register('myAutoload');
// create new slim object
$router = new \Slim\App();
//start DB connection
$conn = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_pass);
$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);