<?php
$includeD3 = true;
include("header.php");
?>

<body>
<div class="container-fluid">
    <h2>NoPap 1.0</h2>
    <ul class="nav nav-pills">
        <li class="active"><a href="#">Fleets</a></li>
        <li><a href="analysis.php">Analysis</a></li>
    </ul>
    <br/>
    <?php
    include("addFleetForm.php");
    ?>
    <h3>Fleets</h3>
    <h4>Filter</h4>

    <div id="fleetList">
        <form class="form" name="fleetFilter">
            <div class="row">
                <div class="col-md-4">
                    <label for="fc">FC</label>
                </div>
                <div class="col-md-4">
                    <select id="fcSelect" name="fcSelect" class="form-control" onchange="filterFleets(fleetView)">
                        <option value="" selected>Please choose</option>
                        <option v-for="fc in items.fcs" value="{{*fc}}">{{*fc}}</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="fc">Type</label>
                </div>
                <div class="col-md-4">
                    <select id="filterFleetType" name="filterFleetType" class="form-control">
                        <option value="" selected>Please choose</option>
                        <option value="Stratop">Stratop</option>
                        <option value="CTA">CTA</option>
                        <option value="Home Defense">Home Defense</option>
                        <option value="Roam">Roam</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
            </div>
        </form>

        <div class="row">
            <div class="col-md-12">
                <div class="panel-group" id="accordion" style="overflow:hidden">
                    <div v-for="fleet in items.fleets" class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading{{*fleet.ID}}">
                            <a href = "handleFleet.php/delete/{{*fleet.ID}}"><span class="glyphicon glyphicon-trash" style="float:right;"></span></a>
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$index}}">
                                    {{*fleet.timeStr}} / {{*fleet.fleettype}} / {{*fleet.FC}}
                                </a>
                            </h4>

                        </div>
                        <div id="collapse{{$index}}" class="panel-collapse collapse in" style="overflow:hidden">
                            <div class="panel-body">
                                <div id="chart{{$index}}" class="center-block" style="text-align:center;margin:0 auto;height:350px;width:600px;"></div>
                                <table class="table table-striped table-condensed">
                                    <tr>
                                        <th>Pilot</th>
                                        <th>Ship</th>
                                        <th>Location</th>
                                        <th>Corp</th>
                                    </tr>
                                    <tr v-for="attendant in items.fleets[$index].attendants">
                                        <td>{{*attendant.name}}</td>
                                        <td>{{*attendant.ship}}</td>
                                        <td>{{*attendant.location}}</td>
                                        <td>{{*attendant.corp}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <pagination view="fleetView" :pages="pages" :current-page="currentPage"></pagination>
                <br />
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="js/index.js"></script>
</html>