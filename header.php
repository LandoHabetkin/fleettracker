<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <title>Nopap</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="js/vue.min.js"></script>
    <script type="text/javascript" src="js/viewManager.js"></script>
    <?php if ($includeD3 == true) { ?>
        <script type="text/javascript" src="js/d3.min.js"></script>
        <script type="text/javascript" src="js/d3pie.js"></script>
        <!-- Load c3.css -->
        <link href="js/nvd/nv.d3.css" rel="stylesheet" type="text/css">
        <script src="js/nvd/nv.d3.min.js"></script>

    <?php
    }
    ?>
    <link href="css/material.css" rel="stylesheet">
    <link href="js/datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
</head>