<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#addFleet" aria-expanded="false"
        aria-controls="addFleet">
    Add Fleet
</button>
<div id="addFleet" class="collapse">
    <h4>Add fleet</h4>

    <form id="form" name="form" method="post" action="fleets.php/addFleetData" >

        <div class="form-group row" style="text-align:center">
            <div class="col-md-3">
                <label class="control-label" for="fc">FC:</label>
            </div>
            <div class="col-md-5" style="text-align:left">
                <input type="text" class="form-control" name="fc" id="fc"/ style="border:1px dashed #666666;">

            </div>
        </div>
        <div class="form-group row" style="text-align:center">
            <div class="col-md-3">
                <label class="control-label" for="type">Type:</label>
            </div>
            <div class="col-md-5" style="text-align:left">
                <select id="type" name="type" class="form-control" style="border:1px dashed #666666;">
                    <option value="" selected>Please choose</option>
                    <option value="Stratop">Stratop</option>
                    <option value="CTA">CTA</option>
                    <option value="Home Defense">Home Defense</option>
                    <option value="Roam">Roam</option>
                    <option value="Other">Other</option>
                </select>
            </div>
        </div>
        <div class="form-group row" style="text-align:center">
            <div class="col-md-3">
                <label class="control-label" for="date">Date:</label>
            </div>
         <div class="col-md-5" style="text-align:left">
                <input type="text" class="form-control" name="date" id="date" style="border:1px dashed #666666;">
             <script type="text/javascript">
                 $('#date').datepicker({
                     format: "dd.mm.yyyy",
                     autoclose: true,
                     todayHighlight: true
                 });
             </script>

         </div>
        </div>
        <div class="form-group row" style="text-align:center">
            <div class="col-md-3">
                <label class="control-label" for="fleetcomp">Fleet comp window</label>
            </div>
            <div class="col-md-5" style="text-align:left">
                <textarea class="form-control" name="fleetcomp" id="fleetcomp" rows="5" cols="90" required style="border:1px dashed #666666;"></textarea>

            </div>
        </div>
        <!-- submit -->
        <div class="form-group row">
            <div class="col-md-10 col-md-offset-2">
                <button class="btn btn-primary" type="submit" onfocus="this.blur()">Submit</button>
                <button type="reset" class="btn" data-toggle="collapse" data-target="#addinvoiceform" onfocus="this.blur()">Reset</button>
            </div>
        </div>
    </form>
</div>